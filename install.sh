
# useless sudo command to get the root access
echo "Please only give root rights to software you trust. This software is open source and under a free licence. Please consider having a look at the code.
Asking for root rights:"
sudo echo

if [[ ! -d tmp ]]
then
	mkdir tmp
fi


# install dependencies listed in dependency file
echo "installing dependencies"
for pack in $(cat dependencies | sed '{:a;N;$!ba;s_\n_ _}')
do

	a=$(dpkg -s $pack 2> tmp/installed-packages)
	test_if_installed="$(cat tmp/installed-packages | head -n 1 | sed 's_\(.\{10\}\).*_\1_')"
	if [[ "$test_if_installed" == "dpkg-query" ]]
	then
		echo installing $pack
		b=$(sudo apt-get install  $pack 2> tmp/install-status)
		i_status=$(head -n 1 tmp/install-status | sed 's_\(.\{27\}\).*_\1_')
		if [[ "$i_status" == "E: Unable to locate package" ]]
		then
        		echo $pack "was not found in the software repositories. Blazegraph can be installed from http://blazegraph.com"
		fi

	else 
		echo $pack "was already installed"
	fi

done

erase_existing_data="y"

# possibly erase existing data
if [[ -d  ${GLOBAL_data_directory} ]]
then
	echo "existing data in" ${GLOBAL_data_directory} ". Type  [y] to erase it and install. (default: keep data and abort installation)"
	read erase_existing_data
fi

# install
if [[ $erase_existing_data == "y" ]]
then

	# create data directory if not existing
	if [[ ! -d ${GLOBAL_data_directory} ]]
	then
		mkdir ${GLOBAL_data_directory} ${GLOBAL_data_directory}/tmp
	fi 
	
	# substitute variables in binary file and add to PATH
	cat configuration $TOOL.sh > $TOOL
	sudo mv $TOOL ${GLOBAL_executable_file_path}/$TOOL
        sudo chmod -u+rwx ${GLOBAL_executable_file_path}/$TOOL

	# fill data directory
	cp -rf * ${GLOBAL_data_directory}/.  # copy all these files to the data directory

	echo "local data is stored in" ${GLOBAL_data_directory}   
	echo "Now run a RDF graph database and make use of the commands: $TOOL…"

	else 
	echo "software not installed"

fi

