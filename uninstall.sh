
# not removing blazegraph
echo "Blazegraph will not be removed as you may have used it in other projects. If you want to remove it, please do so by yourself."

# remove executable
if [[ -f ${GLOBAL_executable_file_path}/cb ]]
then
sudo rm ${GLOBAL_executable_file_path}/cb
else
echo "no executable found. Did you move to somewhere else? (executable may remain in folder system)"
fi
# remove data directory
if [[ -d ${GLOBAL_data_directory} ]]
then
rm -rf ${GLOBAL_data_directory}
else
echo "no data directory found. Did you move it somewhere else? (data may remain in folder system)"
fi

