this-environment-install.sh: install.sh
	cat configuration install.sh > this-environment-install.sh

.PHONY: install
install :
	bash this-environment-install.sh
	make clean

.PHONY: uninstall
uninstall :
	cat configuration uninstall.sh > this-environment-uninstall.sh
	bash this-environment-uninstall.sh

.PHONY: clean
clean :
	rm this-environment-install.sh this-environment-uninstall.sh -rf tmp
