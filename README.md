
# Publi

## Description

A program that enables to write a table of contents and get a publication made from the notes you may have written precedently.

The notes must contain LinkedData and to achieve this, [hit](https://gitlab.com/jibe-b/hit) and [smd](https://jibe-b.gitlab.io/project/smd) are provided.

The table of contents makes use of the Linked Data of the notes (both content and medatada); to write a table of contents, please learn with the examples [here](https://jibe-b.gitlab.io/project/publi/examples.html).

The resulting document is available in:
- Markdown
- HTML (dokie.li template)
- SMd

Please make use of [smd](/project/smd) (all features are implemented but not all formats are supported) or [pandoc](https://pandoc.org) (the most advanced features are not implemented but more formats are supported) to get the publication in other formats.



## User manual

- write notes in SMd
- write a TOC




### TOC

The table of contents follows the dokie.li template concerning the structure of sections.

For each section, the content is obtained through a few types of SPARQL queries:

- queries matching the content of a note
- queries matching the metadata of a note

## (possible) Roadmap

- dev a visual interface to create the table of contents (hierarchy of frames -> hasPart, frames -> SPARQL)
