#!/bin/bash

GLOBAL_data_directory=/home/u/.cb

# upload notes

bash ../smd/SMD.sh --to trig -f LabNbEntries.smd > notes.trig

. ../cb/cb.sh l j f notes.trig

# upload table of contents

. ../cb/cb.sh l j f toc.trig

# next step is executing publi to build draft publication

. ../cb/cb.sh r j f get-section-description.sparql > section-queries.json

i=0
#for q in section-queries.json
#do

cat section-queries.json | jq '.results.bindings['$i'].section_sparql.value' | sed 's_^"__;s_"$__;s_\\n_\n_g;s_\\"_"_g' >  $i.sparql

. ../cb/cb.sh r j f $i.sparql | jq '.results.bindings[0].txt.value' | sed 's_^"__;s_"$__;s_\\n_\n_g;s_\\"_"_g'  >> brouillon.md
#done


cat brouillon.md


